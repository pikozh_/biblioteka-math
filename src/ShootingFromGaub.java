import java.util.Scanner;

public class ShootingFromGaub {
    public static double distanceInRadians(double alpha, double speed) {
        return ((speed * speed) / 9.8) * Math.sin(Math.toRadians(2 * alpha));
    }

    public static double distanceInDegrees(double alpha, double speed) {
        return ((speed * speed) / 9.8) * Math.sin(Math.toDegrees(2 * alpha));
    }

    public static void workWithUserShootingFromGaub() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите угол возвышения ствола в радианах:");
        double alphaInRadians = Double.parseDouble(scanner.nextLine());

        System.out.println("Введите угол возвышения ствола в градусах:");
        double alphaInDegrees = Double.parseDouble(scanner.nextLine());

        System.out.println("Введите начальную скорость полёта снаряда:");
        double speed = Double.parseDouble(scanner.nextLine());

        System.out.println("Расстояние полета снаряда:\n" + distanceInRadians(alphaInRadians, speed) + " - это решение для угла в радианах;\n" +
                distanceInDegrees(alphaInDegrees, speed) + " - это решение для угла в градусах");


    }
}
