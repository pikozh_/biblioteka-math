import java.util.Scanner;

public class DistanceBetweenCars {
    public static double distance(double speedFirstAuto, double speedSecondAuto, double startDistance, double time) {
        return startDistance + (speedFirstAuto * time) + (speedSecondAuto * time);
    }

    public static void workWithUserDistanceBetweenCars() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите скорость первого автомобиля в км/ч:");
        double speedOfTheFirstAuto = Double.parseDouble(scanner.nextLine());

        System.out.println("Введите скорость второго автомобиля в км/ч:");
        double speedOfTheSecondAuto = Double.parseDouble(scanner.nextLine());

        System.out.println("Введите начальное расстояние между автомобилями в км:");
        double startDistanceBetweenCars = Double.parseDouble(scanner.nextLine());

        System.out.println("Введите время(ч) через которое хотите узнать расстояние между ними:");
        double time = Double.parseDouble(scanner.nextLine());

        System.out.println("Через " + time + " часов расстояние между автомобилями будет: " + distance(speedOfTheFirstAuto, speedOfTheSecondAuto, startDistanceBetweenCars, time) + " км.");


    }
}
