import java.util.Scanner;

public class ShadedArea {
    public static int dotPosition(double x, double y) {
        return (x >= 0) && (y >= 1.5 * x - 1) && (y <= x) || (x <= 0) && (y >= -1.5 * x - 1) && (y <= -x) ? 1 : 0;
    }

    public static void workWithUserShadedArea() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите Х-координаты точки, которую желаете проверить:");
        double x = Double.parseDouble(scanner.nextLine());

        System.out.println("Введите Y-координаты точки, которую желаете проверить:");
        double y = Double.parseDouble(scanner.nextLine());

        System.out.println("Если точка не лежит внутри заштрихованной области, то результат будет 0, иначе - 1\nВнимание, результат: " + dotPosition(x, y));
    }
}
