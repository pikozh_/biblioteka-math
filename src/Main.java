
public class Main {
    public static void main(String[] args) {

        System.out.println("Задание 1. Стрельба из гаубицы");
        ShootingFromGaub.workWithUserShootingFromGaub();

        System.out.println("Задание 2. Расстояние между автомобилями");
        DistanceBetweenCars.workWithUserDistanceBetweenCars();

        System.out.println("Задание 3. Точка в заштрихованной области");
        ShadedArea.workWithUserShadedArea();

        System.out.println("Задание 4. Уравнение");
        Equation.workWithUserEquation();
    }
}
