import java.util.Scanner;

public class Equation {
    public static double solutionOfEquation(double x) {
        return ((6 * Math.log10(Math.sqrt(Math.exp(x + 1) + 2 * Math.exp(x) * Math.cos(x)))) / (Math.log10(x - Math.exp(x + 1) * Math.sin(x)))) + Math.abs((Math.cos(x)) / (Math.exp(Math.sin(x))));
    }

    public static void workWithUserEquation() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите X, чтобы решить уравнение");
        double x = Double.parseDouble(scanner.nextLine());

        System.out.println("Результат вычисления уравнения: " + solutionOfEquation(x));
    }
}
